<?php

use Illuminate\Database\Migrations\Migration;

class IncreaseLocaleLength extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(config('translator.connection'))->table('TranslatorLanguages', function ($table) {
            $table->string('Locale', 10)->change();
        });
        Schema::connection(config('translator.connection'))->table('TranslatorTranslations', function ($table) {
            $table->string('Locale', 10)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
