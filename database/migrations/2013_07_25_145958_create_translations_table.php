<?php

use Illuminate\Database\Migrations\Migration;

class CreateTranslationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(config('translator.connection'))->create('TranslatorTranslations', function ($table) {
            $table->increments('Id');
            $table->string('Locale', 6);
            $table->foreign('Locale')->references('Locale')->on('TranslatorLanguages');

            $table->string('Namespace', 150)->default('*');
            $table->string('Group', 150);
            $table->string('Item', 150);
            $table->text('Text');
            $table->boolean('Unstable')->default(false);
            $table->boolean('Locked')->default(false);
            $table->timestampTz('CreatedAt')->useCurrent();
            $table->timestampTz('UpdatedAt')->nullable()->default(null);

            $table->unique(['Locale', 'Namespace', 'Group', 'Item']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('TranslatorTranslations');
    }
}
