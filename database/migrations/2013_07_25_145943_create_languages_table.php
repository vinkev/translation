<?php

use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(config('translator.connection'))->create('TranslatorLanguages', function ($table) {
            $table->increments('Id');
            $table->string('Locale', 6)->unique();
            $table->string('Name', 60)->unique();
            $table->timestampTz('CreatedAt')->useCurrent();
            $table->timestampTz('UpdatedAt')->nullable()->default(null);
            $table->timestampTz('DeletedAt')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('TranslatorLanguages');
    }

}
