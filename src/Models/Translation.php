<?php namespace Waavi\Translation\Models;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    /**
     *  Table name in the database.
     *  @var string
     */
    protected $table = 'TranslatorTranslations';

    /**
     *  List of variables that can be mass assigned
     *  @var array
     */
    protected $fillable = ['Locale', 'Namespace', 'Group', 'Item', 'Text', 'Unstable'];

    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';

    protected $primaryKey = 'Id';

    protected $dates = ['CreatedAt', 'UpdatedAt'];
    protected $hidden = ['CreatedAt', 'UpdatedAt'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('translator.connection'));
    }

    /**
     *  Each translation belongs to a language.
     */
    public function language()
    {
        return $this->belongsTo(Language::class, 'Locale', 'Locale');
    }

    /**
     *  Returns the full translation code for an entry: namespace.group.item
     *  @return string
     */
    public function getCodeAttribute()
    {
        return $this->Namespace === '*' ? "{$this->Group}.{$this->Item}" : "{$this->Namespace}::{$this->Group}.{$this->Item}";
    }

    /**
     *  Flag this entry as Reviewed
     *  @return void
     */
    public function flagAsReviewed()
    {
        $this->Unstable = 0;
    }

    /**
     *  Set the translation to the locked state
     *  @return void
     */
    public function lock()
    {
        $this->Locked = 1;
    }

    /**
     *  Check if the translation is locked
     *  @return boolean
     */
    public function isLocked()
    {
        return (boolean) $this->Locked;
    }
}
