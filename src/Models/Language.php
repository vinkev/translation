<?php namespace Waavi\Translation\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    /**
     *  Table name in the database.
     *  @var string
     */
    protected $table = 'TranslatorLanguages';

    /**
     *  List of variables that cannot be mass assigned
     *  @var array
     */
    protected $fillable = ['Locale', 'Name'];

    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
    const DELETED_AT = 'DeletedAt';

    protected $primaryKey = 'Id';

    protected $dates = ['CreatedAt', 'UpdatedAt', 'DeletedAt'];
    protected $hidden = ['CreatedAt', 'UpdatedAt', 'DeletedAt'];

    /**
     * Language constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(config('translator.connection'));
    }

    /**
     *  Each language may have several translations.
     */
    public function translations()
    {
        return $this->hasMany(Translation::class, 'Locale', 'Locale');
    }

    /**
     *  Returns the name of this language in the current selected language.
     *
     *  @return string
     */
    public function getLanguageCodeAttribute()
    {
        return "languages.{$this->Locale}";
    }

}
